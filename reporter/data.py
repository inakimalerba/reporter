"""Data format to be passed to the template."""
from urllib.request import urlopen

from cached_property import cached_property

from . import settings


class TestData:
    """Data about all revision's tests."""

    def __init__(self, build_data):
        """Initialize."""
        tests = []
        for build in build_data.builds:
            build_tests = build.tests.list(as_list=True)
            for test in build_tests:
                # Overwriting test.issues because after getting a list of all
                # issues, we shouldn't need them anymore
                test.issues = test.issues.list(as_list=True)
                test.architecture = build.architecture

            tests += build_tests

        self.tests = tests

    def _tests_filtered(self, condition, exclude_waived=False):
        tests = self.tests
        if exclude_waived:
            tests = filter(lambda x: not x.waived, tests)
        return list(filter(condition, tests))

    @cached_property
    def passed_tests(self):
        """Get a list of build's tests that passed."""
        return self._tests_filtered(lambda test: test.status == 'PASS')

    @cached_property
    def skipped_tests(self):
        """Get a list of build's tests that got skipped."""
        return self._tests_filtered(lambda test: test.status == 'SKIP')

    @cached_property
    def failed_tests(self):
        """Get a list of build's tests that failed."""
        return self._tests_filtered(lambda test: test.status == 'FAIL',
                                    exclude_waived=True)

    @cached_property
    def errored_tests(self):
        """Get a list of build's tests that encountered an infra error."""
        return self._tests_filtered(lambda test: test.status == 'ERROR')

    @cached_property
    def waived_tests(self):
        """Get a list of waived build's tests."""
        return self._tests_filtered(lambda test: test.waived)

    @cached_property
    def known_issues_tests(self):
        """Get a list of build's tests that failed due to known issues."""
        return self._tests_filtered(lambda test: test.issues)

    @cached_property
    def unknown_issues_tests(self):
        """Get a list of build's tests that failed due to unknown issues."""
        return self._tests_filtered(lambda test: test.status == 'FAIL' and
                                    test not in self.known_issues_tests and
                                    test not in self.waived_tests)


class BuildData:
    """Data about all revision's builds."""

    def __init__(self, builds):
        """Initialize."""
        self.builds = builds.list(as_list=True)
        for build in self.builds:
            # Overwriting build.issues because after getting a list of all
            # issues, we shouldn't need them anymore
            build.issues = build.issues.list(as_list=True)

    @cached_property
    def passed_builds(self):
        """Return passing builds."""
        return list(filter(lambda x: x.valid, self.builds))

    @cached_property
    def failed_builds(self):
        """Return failing builds."""
        return list(filter(lambda x: not x.valid, self.builds))

    @cached_property
    def known_issues_builds(self):
        """Get a list of builds that failed due to known issues."""
        return list(filter(lambda x: x.issues, self.builds))

    @cached_property
    def unknown_issues_builds(self):
        """Get a list of builds that failed due to unknown issues."""
        known_issues = self.known_issues_builds
        return list(filter(lambda x: x not in known_issues,
                           self.failed_builds))


class RevisionData:
    """Data about a single revision."""

    def __init__(self, revision_id):
        """initialize."""
        self.rev = settings.DATAWAREHOUSE.kcidb.revisions.get(id=revision_id)

    @cached_property
    def build_data(self):
        """Get the BuildData object which contains information about builds."""
        return BuildData(self.rev.builds)

    @cached_property
    def test_data(self):
        """Get the TestData object which contains information about tests."""
        return TestData(self.build_data)

    @cached_property
    def issues(self):
        """Return the revision's known issues."""
        return self.rev.issues.list(as_list=True)

    @cached_property
    def nvr(self):
        """Return the revision's NVR."""
        return self.rev.misc['nvr']

    @cached_property
    def mergelog(self):
        """Return the revision's mergelog file."""
        with urlopen(self.rev.log_url) as log:
            return log.read().decode('utf-8')

    @cached_property
    def result(self):
        """
        Get the overall result.

        True if everything passed, False otherwise.
        """
        return (self.rev.valid and
                self.build_data.unknown_issues_builds == [] and
                self.test_data.unknown_issues_tests == [])
