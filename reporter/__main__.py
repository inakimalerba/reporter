"""Main reporter definition."""
from argparse import ArgumentParser
import os
import sys

from cki_lib import misc
from cki_lib.retrying import retrying_on_exception
import sentry_sdk

from . import settings
from .data import RevisionData
from .utils import status_to_emoji
from .utils import yesno

settings.JINJA_ENV.filters['status_to_emoji'] = status_to_emoji
settings.JINJA_ENV.filters['yesno'] = yesno


class ReporterException(Exception):
    """An exception in reporting."""


@retrying_on_exception(Exception, retries=settings.RETRY_COUNT,
                       initial_delay=settings.RETRY_DELAY)
def render_template(revision_id):
    """Render the report template using revision data."""
    revision_data = RevisionData(revision_id)

    template = settings.JINJA_ENV.get_template(settings.TEMPLATE_NAME)
    context = {'revision_data': revision_data,
               'build_data': revision_data.build_data,
               'test_data': revision_data.test_data,
               'dw_url': settings.DATAWAREHOUSE_URL}
    print(template.render(context).strip())
    return int(not revision_data.result)


# pylint: disable=unused-argument
def process_message(routing_key, payload):
    """Process the webhook message."""
    settings.LOGGER.info('processing message (payload): %s', payload)
    if payload['status'] != 'ready_to_report':
        settings.LOGGER.info('Skipping status: "%s"', payload['status'])
        return
    if 'object_type' in payload and payload['object_type'] == 'revision' and \
       'id' in payload:
        render_template(payload['id'])
    else:
        error = 'Invalid "ready_to_report" message recieved'
        settings.LOGGER.error(error)
        raise ReporterException(error)


def create_parser():
    """Create argument parser."""
    parser = ArgumentParser('Create reports for provided pipelines')
    parser.add_argument('-t', '--template', type=str,
                        metavar='TEMPLATE_FILENAME',
                        help='Template filename relative to the ' +
                        'template folder. The file must be inside the ' +
                        'template folder for custom filters and imports ' +
                        'to work.')
    group = parser.add_mutually_exclusive_group(required=False)
    group.add_argument('-l', '--listen', action='store_true',
                       help='Start polling for rabbitmq messages and create ' +
                       'reports for ready_to_report revisions.' +
                       'Defaults to this if no other option was selected.')
    group.add_argument('-r', '--revision_id', type=str,
                       help='Specify the revision id of a revision ' +
                       'you want to create a single report for.')
    return parser


def main():
    """Set up and start consuming messages."""
    if misc.is_production():
        sentry_sdk.init(ca_certs=os.getenv('REQUESTS_CA_BUNDLE'))
    parser = create_parser()
    args = parser.parse_args()

    if args.template is not None:
        settings.TEMPLATE_NAME = args.template

    if args.revision_id:
        return_code = render_template(args.revision_id)
        # exit code based on report result for local runs
        if not misc.is_production():
            sys.exit(return_code)
    else:
        # Default to rabbitmq consuming
        print('Now consuming Rabbitmq messages')
        settings.QUEUE.consume_messages(
            settings.REPORTER_EXCHANGE,
            ['#'],
            process_message,
            queue_name=settings.REPORTER_QUEUE
        )


if __name__ == '__main__':
    main()
