"""Initialize the message queue."""
import os
from pathlib import Path

from cki_lib import misc
from cki_lib.logger import get_logger
from cki_lib.messagequeue import MessageQueue
from datawarehouse import Datawarehouse
from jinja2 import Environment
from jinja2 import FileSystemLoader
from jinja2 import StrictUndefined

REPORTER_EXCHANGE = os.environ.get('REPORTER_EXCHANGE',
                                   'cki.exchange.datawarehouse.kcidb')
REPORTER_QUEUE = os.environ.get('REPORTER_QUEUE',
                                'cki.queue.datawarehouse.kcidb.reporter')

QUEUE = MessageQueue()

DATAWAREHOUSE_URL = os.environ.get('DATAWAREHOUSE_URL', 'http://localhost')
DATAWAREHOUSE_TOKEN = os.environ.get('DATAWAREHOUSE_TOKEN_REPORTER')

DATAWAREHOUSE = Datawarehouse(DATAWAREHOUSE_URL, token=DATAWAREHOUSE_TOKEN)

LOGGER = get_logger('cki.reporter')

RETRY_COUNT = misc.get_env_int('RETRY_COUNT', 10)
RETRY_DELAY = misc.get_env_int('RETRY_DELAY', 15)

TEMPLATE_DIR = Path(__file__).resolve().parent / 'templates'
TEMPLATE_NAME = 'report.j2'

# Set up the jinja2 environment which can be reused throughout this script.
JINJA_ENV = Environment(
    loader=FileSystemLoader(TEMPLATE_DIR),
    trim_blocks=True,  # Remove first newline after a jinja2 block
    lstrip_blocks=True,  # Strip whitespace from the left side of tags
    undefined=StrictUndefined,
)
