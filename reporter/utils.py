"""Utility functions like custom jinja filters."""

EMOJI_MAP = {
    'PASS': '✅',
    'DONE': '✅',
    'FAIL': '❌',
    'ERROR': '⚡⚡⚡'
}


def status_to_emoji(status):
    """Convert test status to emoji."""
    return EMOJI_MAP[status]


def yesno(condition, values="yes,no"):
    """Return first value if True, second if False, third if None."""
    # Mimics the django templates builtin filter
    # https://docs.djangoproject.com/en/3.0/ref/templates/builtins/#yesno
    values = values.split(',')
    if condition:
        return values[0]

    if condition is None and len(values) > 2:
        return values[2]

    return values[1]
