from unittest import TestCase
from unittest.mock import MagicMock
from unittest.mock import patch

from reporter.data import BuildData
from reporter.data import RevisionData
from reporter.data import TestData

from .mocks import get_mocked_build
from .mocks import get_mocked_revision
from .mocks import get_mocked_test

TestData.__test__ = False  # To supress a pytest warning


class TestReporterData(TestCase):
    """Tests for reporter/data.py ."""

    @patch('reporter.data.urlopen')
    @patch('reporter.settings.DATAWAREHOUSE.kcidb.revisions.get')
    def test_revision_data(self, revisions_get_mock, urlopen_mock):
        """Test the RevisionData class and it's properties."""
        tests = [get_mocked_test('PASS', False)]
        builds = [get_mocked_build(True, tests)]
        issues = ['Issue']
        revision = get_mocked_revision(True, builds, issues)
        revision.misc = {'nvr': 'n-v-r'}

        revision_id = '123'
        revisions_get_mock.return_value = revision
        revision_data = RevisionData(revision_id)
        revisions_get_mock.assert_called_with(id=revision_id)

        self.assertEqual(revision_data.rev, revision)
        self.assertEqual(revision_data.nvr, 'n-v-r')

        merge_log = MagicMock()
        merge_log.read.side_effect = [b'bar']
        urlopen_mock.return_value.__enter__.return_value = merge_log

        self.assertEqual(revision_data.mergelog,
                         'bar')

        self.assertEqual(revision_data.issues, issues)

        self.assertEqual(revision_data.build_data.builds, builds)
        self.assertEqual(revision_data.test_data.tests, tests)

        self.assertTrue(revision_data.result)

    @patch('reporter.settings.DATAWAREHOUSE.kcidb.revisions.get')
    def test_revision_data_failed_build_result(self, revisions_get_mock):
        """Test the RevisionData result property."""
        tests = []
        builds = [get_mocked_build(True, tests),
                  get_mocked_build(False, tests),
                  get_mocked_build(False, tests, ['Issue'])]
        revision = get_mocked_revision(True, builds)

        revisions_get_mock.return_value = revision
        revision_data = RevisionData('1234')

        self.assertFalse(revision_data.result)

    @patch('reporter.settings.DATAWAREHOUSE.kcidb.revisions.get')
    def test_revision_data_known_issue_build_result(self, revisions_get_mock):
        """Test the RevisionData result property."""
        tests = []
        builds = [get_mocked_build(True, tests),
                  get_mocked_build(False, tests, ['Issue'])]
        revision = get_mocked_revision(True, builds)

        revisions_get_mock.return_value = revision
        revision_data = RevisionData('1234')

        self.assertTrue(revision_data.result)

    def test_build_data(self):
        """Test the BuildData class and it's properties."""
        tests = []
        passed_build = get_mocked_build(True, tests)
        failed_build = get_mocked_build(False, tests)
        known_failure_build = get_mocked_build(False, tests, ['Issue'])

        builds = MagicMock()
        builds.list.return_value = [passed_build, failed_build,
                                    known_failure_build]

        build_data = BuildData(builds)

        self.assertEqual(build_data.passed_builds, [passed_build])
        self.assertEqual(build_data.failed_builds,
                         [failed_build, known_failure_build])
        self.assertEqual(build_data.known_issues_builds,
                         [known_failure_build])
        self.assertEqual(build_data.unknown_issues_builds, [failed_build])

    def test_test_data(self):
        """Test the TestData class and it's properties."""
        pass_test = get_mocked_test('PASS', False)
        pass_waived_test = get_mocked_test('PASS', True)
        fail_test = get_mocked_test('FAIL', False)
        fail_waived_test = get_mocked_test('FAIL', True)
        error_test = get_mocked_test('ERROR', False)
        skip_test = get_mocked_test('SKIP', False)
        fail_known_test = get_mocked_test('FAIL', False, ['issue'])
        tests = [pass_test, fail_test, pass_waived_test, fail_waived_test,
                 error_test, skip_test, fail_known_test]

        builds = MagicMock()
        builds.list.return_value = [get_mocked_build(True, tests)]

        build_data = BuildData(builds)
        test_data = TestData(build_data)

        self.assertEqual(test_data.tests, tests)

        self.assertEqual(test_data.failed_tests,
                         [fail_test, fail_known_test])
        self.assertEqual(test_data.passed_tests,
                         [pass_test, pass_waived_test])
        self.assertEqual(test_data.skipped_tests, [skip_test])
        self.assertEqual(test_data.errored_tests, [error_test])
        self.assertEqual(test_data.waived_tests,
                         [pass_waived_test, fail_waived_test])
        self.assertEqual(test_data.known_issues_tests, [fail_known_test])
        self.assertEqual(test_data.unknown_issues_tests,
                         [fail_test])  # I'm not expecting the waived test here
